import static org.junit.Assert.*;

import org.junit.Test;

import com.example.Application;

public class AppTest {

    @Test
    public void testAppOne() {
        Application myApp = new Application();

        String result = myApp.getStatus();

        assertEquals("OK", result);
    }

    @Test
    public void testAppTwo() {
        Application myApp = new Application();

        boolean result = myApp.getCondition(true);

        assertTrue(result);
    }

}
